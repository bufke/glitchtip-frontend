export const organization = {
  name: "cypresstestorg",
  otherOrg: "cypress-test-org-other",
};

export const team = {
  name: "cypresstestteam",
};

export const newTeam = {
  slug: "newcypresstestteam",
};

export const project = {
  name: "cypresstestproject",
};

export const newProject = {
  name: "newcypresstestproject",
  platform: "newcypresstestplatform",
};
