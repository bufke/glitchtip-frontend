You selected "other", which means that there might not be a SDK for you.

[Many SDKs are available on GitHub](https://github.com/getsentry/) though, so you might be able to find what you're looking for there. If you'd like, you can edit the project settings to change the platform.
