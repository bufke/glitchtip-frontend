To start using GlitchTip, you will need to:

1. Install a library into your project so it is able to send events to GlitchTip.
2. Configure it with a Data Source Name (DSN) so it knows where exactly to send events. Your DSN is below, and can also be found in the project's settings.
3. Initialize the library with your DSN to start sending events.
