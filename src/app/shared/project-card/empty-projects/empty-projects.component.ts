import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
  selector: "app-empty-projects",
  templateUrl: "./empty-projects.component.html",
  styleUrls: ["./empty-projects.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmptyProjectsComponent {}
