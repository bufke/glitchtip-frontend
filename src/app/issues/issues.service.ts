import { Injectable } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { MatSnackBar } from "@angular/material/snack-bar";
import { BehaviorSubject, combineLatest, Observable, EMPTY } from "rxjs";
import { tap, catchError, map } from "rxjs/operators";
import { Issue, IssueWithSelected, IssueStatus } from "./interfaces";
import { urlParamsToObject } from "./utils";
import { processLinkHeader } from "../shared/utils-pagination";
import { IssuesAPIService } from "../api/issues/issues-api.service";

interface IssuesState {
  issues: Issue[];
  selectedIssues: number[];
  issueCount: number | null;
  page: number | null;
  nextPage: string | null;
  previousPage: string | null;
  loading: boolean;
  initialLoadComplete: boolean;
}

const initialState: IssuesState = {
  issues: [],
  selectedIssues: [],
  issueCount: null,
  page: null,
  nextPage: null,
  previousPage: null,
  loading: false,
  initialLoadComplete: false,
};

@Injectable({
  providedIn: "root",
})
export class IssuesService {
  private issuesState = new BehaviorSubject<IssuesState>(initialState);
  private getState$ = this.issuesState.asObservable();

  issues$ = this.getState$.pipe(map((state) => state.issues));
  selectedIssues$ = this.getState$.pipe(map((state) => state.selectedIssues));
  issuesWithSelected$: Observable<IssueWithSelected[]> = combineLatest([
    this.issues$,
    this.selectedIssues$,
  ]).pipe(
    map(([issues, selectedIssues]) =>
      issues.map((issue) => ({
        ...issue,
        isSelected: selectedIssues.includes(issue.id) ? true : false,
        projectSlug: issue.project?.slug,
      }))
    )
  );
  areAllSelected$ = combineLatest([this.issues$, this.selectedIssues$]).pipe(
    map(([issues, selectedIssues]) => issues.length === selectedIssues.length)
  );
  readonly thereAreSelectedIssues$ = this.selectedIssues$.pipe(
    map((selectedIssues) => selectedIssues.length > 0)
  );
  issueCount$ = this.getState$.pipe(map((state) => state.issueCount));
  hasNextPage$ = this.getState$.pipe(map((state) => state.nextPage !== null));
  hasPreviousPage$ = this.getState$.pipe(
    map((state) => state.previousPage !== null)
  );
  nextPageParams$ = this.getState$.pipe(
    map((state) => urlParamsToObject(state.nextPage))
  );
  previousPageParams$ = this.getState$.pipe(
    map((state) => urlParamsToObject(state.previousPage))
  );
  loading$ = this.getState$.pipe(map((state) => state.loading));
  initialLoadComplete$ = this.getState$.pipe(
    map((state) => state.initialLoadComplete)
  );

  constructor(
    private snackbar: MatSnackBar,
    private issuesAPIService: IssuesAPIService
  ) {}

  /** Refresh issues data. orgSlug is required. */
  getIssues(
    orgSlug: string,
    cursor: string | undefined,
    query: string = "is:unresolved",
    project: string[] | null,
    start: string | undefined,
    end: string | undefined
  ) {
    this.setLoading(true);
    this.retrieveIssues(
      orgSlug,
      cursor,
      query,
      project,
      start,
      end
    ).toPromise();
  }

  toggleSelected(issueId: number) {
    const state = this.issuesState.getValue();
    let selectedIssues: number[];
    if (state.selectedIssues.includes(issueId)) {
      selectedIssues = state.selectedIssues.filter(
        (issue) => issue !== issueId
      );
    } else {
      selectedIssues = state.selectedIssues.concat([issueId]);
    }
    this.issuesState.next({ ...state, selectedIssues });
  }

  toggleSelectAll() {
    const state = this.issuesState.getValue();
    if (state.issues.length === state.selectedIssues.length) {
      this.issuesState.next({
        ...state,
        selectedIssues: [],
      });
    } else {
      this.issuesState.next({
        ...state,
        selectedIssues: state.issues.map((issue) => issue.id),
      });
    }
  }

  /** Set one specified issue ID as status */
  setStatus(id: number, status: IssueStatus) {
    return this.updateStatus([id], status);
  }

  /** Set all selected issues to this status */
  bulkSetStatus(status: IssueStatus) {
    const selectedIssues = this.issuesState.getValue().selectedIssues;
    return this.updateStatus(selectedIssues, status).toPromise();
  }

  /** Get issues from backend using appropriate endpoint based on organization */
  private retrieveIssues(
    organizationSlug?: string,
    cursor?: string,
    query?: string,
    project?: string[] | null,
    start?: string,
    end?: string
  ) {
    return this.issuesAPIService
      .list(organizationSlug, cursor, query, project, start, end)
      .pipe(
        tap((resp) => {
          const linkHeader = resp.headers.get("link");
          if (resp.body && linkHeader) {
            this.setIssues(resp.body);
            this.setPagination(linkHeader);
          }
        })
      );
  }

  clearState() {
    this.issuesState.next(initialState);
  }

  private updateStatus(ids: number[], status: IssueStatus) {
    return this.issuesAPIService.update(ids, status).pipe(
      tap((resp) => this.setIssueStatuses(ids, resp.status)),
      catchError((err: HttpErrorResponse) => {
        this.snackbar.open("Error, unable to update issue");
        return EMPTY;
      })
    );
  }

  private setIssueStatuses(issueIds: number[], status: IssueStatus) {
    const state = this.issuesState.getValue();
    this.issuesState.next({
      ...state,
      issues: state.issues.map((issue) =>
        issueIds.includes(issue.id) ? { ...issue, status } : issue
      ),
      selectedIssues: [],
    });
  }

  private setIssues(issues: Issue[]) {
    this.issuesState.next({
      ...this.issuesState.getValue(),
      issues,
      loading: false,
      initialLoadComplete: true,
    });
  }

  private setLoading(loading: boolean) {
    this.issuesState.next({ ...this.issuesState.getValue(), loading });
  }

  private setPagination(linkHeader: string) {
    const parts = processLinkHeader(linkHeader);
    this.issuesState.next({
      ...this.issuesState.getValue(),
      nextPage: parts.next ? parts.next : null,
      previousPage: parts.previous ? parts.previous : null,
    });
  }
}
